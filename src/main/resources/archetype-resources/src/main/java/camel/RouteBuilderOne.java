#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.camel;

import org.apache.camel.spring.boot.FatJarRouter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ${package}.AppProperties;

@Component
public class RouteBuilderOne extends FatJarRouter {

	@Autowired
	private AppProperties properties;
	
	@Override
	public void configure() throws Exception {
		
		onException(Exception.class).handled(true).stop();
		
		from("wmqIn:queue:"+properties.getMqDestination()).
		log("LOG RECEIVED MESSAGE").
		to("salesforce:"+properties.getSalesforceAction()).
		log("LOG SENT MESSAGE").
		end();
		
	}

}
