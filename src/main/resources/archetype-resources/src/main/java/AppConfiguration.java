#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package};

import javax.jms.JMSException;

import org.apache.camel.CamelContext;
import org.apache.camel.CamelContextAware;
import org.apache.camel.component.jms.JmsComponent;
import org.apache.camel.component.salesforce.SalesforceComponent;
import org.apache.camel.component.salesforce.SalesforceLoginConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jms.connection.CachingConnectionFactory;

import com.ibm.mq.jms.MQConnectionFactory;
import com.ibm.msg.client.wmq.WMQConstants;

@Configuration
public class AppConfiguration implements CamelContextAware {

	@Autowired
	private AppProperties properties;

	private CamelContext context;

	/**
	 * Queue Manager Connection Components
	 * 
	 * @return
	 */
	@Bean
	public MQConnectionFactory wmqCFIn() throws JMSException {

		MQConnectionFactory wmqCFIn = new MQConnectionFactory();
		wmqCFIn.setHostName(properties.getIbmMqHostName());
		wmqCFIn.setPort(properties.getIbmMqPort());
		wmqCFIn.setQueueManager(properties.getIbmMqQueueManager());
		wmqCFIn.setChannel(properties.getIbmMqChannel());
		wmqCFIn.setTransportType(WMQConstants.WMQ_CM_CLIENT);

		return wmqCFIn;
	}

	/**
	 * Spring Caching Connection Factory
	 * 
	 * @return
	 */
	@Bean
	@Primary
	public CachingConnectionFactory wmqCCFIn(MQConnectionFactory wmqCFIn) {

		CachingConnectionFactory wmqCCFIn = new CachingConnectionFactory();
		wmqCCFIn.setTargetConnectionFactory(wmqCFIn);
		wmqCCFIn.setCacheConsumers(properties.isCacheConsumers());
		wmqCCFIn.setReconnectOnException(properties.isReconnectOnException());
		wmqCCFIn.setSessionCacheSize(properties.getSessionCachSize());

		return wmqCCFIn;
	}

	/**
	 * JMS EndPoint Component
	 * 
	 * @return
	 */
	@Bean(name = "wmqIn")
	public JmsComponent wmqIn(CachingConnectionFactory wmqCCFIn) {

		JmsComponent wmqIn = new JmsComponent(context);
		wmqIn.setConnectionFactory(wmqCCFIn);
		wmqIn.setTransacted(properties.isTransacted());
		wmqIn.setCacheLevelName(properties.getCacheLevelName());
		wmqIn.setConcurrentConsumers(properties.getConcurrentConsumers());
		wmqIn.setMaxConcurrentConsumers(properties.getMaxConcurrentConsumers());
		wmqIn.setMaxMessagesPerTask(properties.getMaxMessagesPerTask());
		wmqIn.setAcknowledgementModeName(properties.getAcknowledgementModeName());

		return wmqIn;
	}

	/**
	 * Camel Salesforce EndPoint
	 * 
	 * @return
	 */
	@Bean(name = "salesforce")
	public SalesforceComponent salesforce() {

		SalesforceLoginConfig config = new SalesforceLoginConfig();
		config.setClientId(properties.getSfdcClientId());
		config.setClientSecret(properties.getSfdcClientSecret());
		config.setLoginUrl(properties.getSfdcLoginUrl());
		config.setUserName(properties.getSfdcUserName());
		config.setPassword(properties.getSfdcPassword());

		SalesforceComponent salesforce = new SalesforceComponent(context);
		salesforce.setLoginConfig(config);
		return salesforce;
	};

	public CamelContext getCamelContext() {
		return context;
	}

	public void setCamelContext(CamelContext context) {
		this.context = context;
	}
}
