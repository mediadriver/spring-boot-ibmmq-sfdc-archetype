#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package};

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource("classpath:application.properties")
public class AppProperties {

	// MQ Connection Factory
	@Value("${symbol_dollar}{ibm.mq.hostName}")
	private String ibmMqHostName;

	@Value("${symbol_dollar}{ibm.mq.port}")
	private int ibmMqPort;

	@Value("${symbol_dollar}{ibm.mq.queueManager}")
	private String ibmMqQueueManager;

	@Value("${symbol_dollar}{ibm.mq.Channel}")
	private String ibmMqChannel;

	// Spring Connection Factory
	@Value("${symbol_dollar}{connection.cacheConsumers}")
	private boolean cacheConsumers;

	@Value("${symbol_dollar}{connection.reconnectOnException}")
	private boolean reconnectOnException;

	@Value("${symbol_dollar}{connection.sessionCacheSize}")
	private int sessionCachSize;

	// Camel JMS Component
	@Value("${symbol_dollar}{jms.transacted}")
	private boolean transacted;

	@Value("${symbol_dollar}{jms.cacheLevelName}")
	private String cacheLevelName;

	@Value("${symbol_dollar}{jms.concurrentConsumers}")
	private int concurrentConsumers;

	@Value("${symbol_dollar}{jms.maxConcurrentConsumers}")
	private int maxConcurrentConsumers;

	@Value("${symbol_dollar}{jms.maxMessagesPerTask}")
	private int maxMessagesPerTask;
	
	@Value("${symbol_dollar}{jms.acknowledgementModeName}")
	private String acknowledgementModeName;

	// Salesforce details
	@Value("${symbol_dollar}{sfdc.loginUrl}")
	private String sfdcLoginUrl;

	@Value("${symbol_dollar}{sfdc.clientId}")
	private String sfdcClientId;

	@Value("${symbol_dollar}{sfdc.clientSecret}")
	private String sfdcClientSecret;

	@Value("${symbol_dollar}{sfdc.userName}")
	private String sfdcUserName;

	@Value("${symbol_dollar}{sfdc.password}")
	private String sfdcPassword;
	
	// Route details
	@Value("${symbol_dollar}{ibm.mq.destination}")
	private String mqDestination;
	
	@Value("${symbol_dollar}{salesforce.action}")
	private String salesforceAction;

	public String getIbmMqHostName() {
		return ibmMqHostName;
	}

	public void setIbmMqHostName(String ibmMqHostName) {
		this.ibmMqHostName = ibmMqHostName;
	}

	public int getIbmMqPort() {
		return ibmMqPort;
	}

	public void setIbmMqPort(int ibmMqPort) {
		this.ibmMqPort = ibmMqPort;
	}

	public String getIbmMqQueueManager() {
		return ibmMqQueueManager;
	}

	public void setIbmMqQueueManager(String ibmMqQueueManager) {
		this.ibmMqQueueManager = ibmMqQueueManager;
	}

	public String getIbmMqChannel() {
		return ibmMqChannel;
	}

	public void setIbmMqChannel(String ibmMqChannel) {
		this.ibmMqChannel = ibmMqChannel;
	}

	public boolean isCacheConsumers() {
		return cacheConsumers;
	}

	public void setCacheConsumers(boolean cacheConsumers) {
		this.cacheConsumers = cacheConsumers;
	}

	public boolean isReconnectOnException() {
		return reconnectOnException;
	}

	public void setReconnectOnException(boolean reconnectOnException) {
		this.reconnectOnException = reconnectOnException;
	}

	public int getSessionCachSize() {
		return sessionCachSize;
	}

	public void setSessionCachSize(int sessionCachSize) {
		this.sessionCachSize = sessionCachSize;
	}

	public boolean isTransacted() {
		return transacted;
	}

	public void setTransacted(boolean transacted) {
		this.transacted = transacted;
	}

	public String getCacheLevelName() {
		return cacheLevelName;
	}

	public void setCacheLevelName(String cacheLevelName) {
		this.cacheLevelName = cacheLevelName;
	}

	public int getConcurrentConsumers() {
		return concurrentConsumers;
	}

	public void setConcurrentConsumers(int concurrentConsumers) {
		this.concurrentConsumers = concurrentConsumers;
	}

	public int getMaxConcurrentConsumers() {
		return maxConcurrentConsumers;
	}

	public void setMaxConcurrentConsumers(int maxConcurrentConsumers) {
		this.maxConcurrentConsumers = maxConcurrentConsumers;
	}

	public int getMaxMessagesPerTask() {
		return maxMessagesPerTask;
	}

	public void setMaxMessagesPerTask(int maxMessagesPerTask) {
		this.maxMessagesPerTask = maxMessagesPerTask;
	}

	public String getAcknowledgementModeName() {
		return acknowledgementModeName;
	}

	public void setAcknowledgementModeName(String acknowledgementModeName) {
		this.acknowledgementModeName = acknowledgementModeName;
	}

	public String getSfdcLoginUrl() {
		return sfdcLoginUrl;
	}

	public void setSfdcLoginUrl(String sfdcLoginUrl) {
		this.sfdcLoginUrl = sfdcLoginUrl;
	}

	public String getSfdcClientId() {
		return sfdcClientId;
	}

	public void setSfdcClientId(String sfdcClientId) {
		this.sfdcClientId = sfdcClientId;
	}

	public String getSfdcClientSecret() {
		return sfdcClientSecret;
	}

	public void setSfdcClientSecret(String sfdcClientSecret) {
		this.sfdcClientSecret = sfdcClientSecret;
	}

	public String getSfdcUserName() {
		return sfdcUserName;
	}

	public void setSfdcUserName(String sfdcUserName) {
		this.sfdcUserName = sfdcUserName;
	}

	public String getSfdcPassword() {
		return sfdcPassword;
	}

	public void setSfdcPassword(String sfdcPassword) {
		this.sfdcPassword = sfdcPassword;
	}

	public String getMqDestination() {
		return mqDestination;
	}

	public void setMqDestination(String mqDestination) {
		this.mqDestination = mqDestination;
	}

	public String getSalesforceAction() {
		return salesforceAction;
	}

	public void setSalesforceAction(String salesforceAction) {
		this.salesforceAction = salesforceAction;
	}

}
